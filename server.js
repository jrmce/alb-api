'use strict';

const Hapi = require('hapi');
const Good = require('good');

const Api = require('.');

const server = new Hapi.Server();
server.connection({ port: 8001, host: 'localhost' });

server.register([
    Api,
    {
        register: Good,
        options: {
            reporters: {
                console: [{
                    module: 'good-squeeze',
                    name: 'Squeeze',
                    args: [{
                        response: '*',
                        log: '*'
                    }]
                }, {
                    module: 'good-console'
                }, 'stdout']
            }
        }
    }
], (err) => {

    if (err) {
        throw err;
    }

    server.start(() => {

        server.log('info', 'API Started');
    });
});
