'use strict';

const Hapi = require('hapi');
const Fs = require('fs');
const Path = require('path');

const Db = require('../lib/db');
const Api = require('..');

exports.createServer = function (cb) {

    const server = new Hapi.Server();
    server.connection({ host: 'test' });

    server.register([Api], () => {

        return cb(server);
    });
};

exports.cleanDb = function (cb) {

    const tablesToClean = ['accounts', 'photos'];

    const sql = 'DELETE FROM ??';

    tablesToClean.forEach((table, index) => {

        Db.query(sql, [table], () => {

            if (index === tablesToClean.length - 1) {
                return cb();
            }
        });
    });
};

exports.basicHeader = function (email, password) {

    const buffer = new Buffer(`${email}:${password}`, 'utf8').toString('base64');
    return `Basic ${buffer}`;
};

exports.getPhoto = function (type, cb) {

    if (typeof type === 'function') {
        cb = type;
        type = 'png';
    }

    Fs.readFile(Path.resolve(__dirname, `test_${type}.${type}`), (err, data) => {

        if (err) {
            throw err;
        }

        const base64 = new Buffer(data).toString('base64');
        return cb(base64);
    });
};
