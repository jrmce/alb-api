'use strict';

const Lab = require('lab');

const Util = require('./util');

const lab = exports.lab = Lab.script();
const before = lab.before;
const afterEach = lab.afterEach;
const describe = lab.experiment;
const it = lab.test;
const expect = require('code').expect;

describe('Health handler', () => {

    let server;

    before((done) =>  {

        Util.createServer((s) => {

            server = s;
            return done();
        });
    });

    afterEach((done) => {

        Util.cleanDb(() => {

            return done();
        });
    });

    it('should return success', (done) => {

        server.inject({
            method: 'GET',
            url: '/health'
        }, (res) => {

            expect(res.statusCode).to.be.equal(200);
            expect(res.result).to.be.equal({ status: 'ok' });
            return done();
        });
    });

    it('should return success for authenticated request', (done) => {

        const email = 'test@example.com';
        const password = 'secret';

        server.inject({
            method: 'POST',
            url: '/accounts',
            payload: { email, password }
        }, (res) => {

            server.inject({
                method: 'GET',
                url: '/health/auth',
                headers: {
                    authorization: Util.basicHeader(email, password)
                }
            }, (response) => {

                expect(response.statusCode).to.be.equal(200);
                expect(response.result).to.be.equal({ status: 'ok' });
                return done();
            });
        });
    });

    it('should return unauthorized for a request with bad credentials', (done) => {

        const email = 'test@example.com';
        const password = 'secret';

        server.inject({
            method: 'POST',
            url: '/accounts',
            payload: { email, password }
        }, (res) => {

            server.inject({
                method: 'GET',
                url: '/health/auth',
                headers: {
                    authorization: Util.basicHeader(email, 'wrong')
                }
            }, (response) => {

                expect(response.statusCode).to.be.equal(401);
                return done();
            });
        });
    });
});
