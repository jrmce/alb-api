'use strict';

const Fs = require('fs');
const Lab = require('lab');

const Util = require('./util');

const lab = exports.lab = Lab.script();
const before = lab.before;
const afterEach = lab.afterEach;
const describe = lab.experiment;
const it = lab.test;
const expect = require('code').expect;

describe('Photos handler', () => {

    let server;

    before((done) =>  {

        Util.createServer((s) => {

            server = s;
            return done();
        });
    });

    afterEach((done) => {

        Util.cleanDb(() => {

            return done();
        });
    });

    describe('creating a photo', () => {

        it('should create a png photo', (done) => {

            Util.getPhoto((base64) => {

                server.inject({
                    method: 'POST',
                    url: '/photos',
                    payload: {
                        data: base64
                    },
                    credentials: {}
                }, (res) => {

                    expect(res.statusCode).to.be.equal(201);
                    expect(res.headers.location).to.include('/photos/');
                    expect(res.result.id).to.be.a.number();
                    return done();
                });
            });
        });

        it('should create a jpg photo', (done) => {

            Util.getPhoto('jpg', (base64) => {

                server.inject({
                    method: 'POST',
                    url: '/photos',
                    payload: {
                        data: base64
                    },
                    credentials: {}
                }, (res) => {

                    expect(res.statusCode).to.be.equal(201);
                    expect(res.headers.location).to.include('/photos/');
                    expect(res.result.id).to.be.a.number();
                    return done();
                });
            });
        });

        it('should create a gif photo', (done) => {

            Util.getPhoto('gif', (base64) => {

                server.inject({
                    method: 'POST',
                    url: '/photos',
                    payload: {
                        data: base64
                    },
                    credentials: {}
                }, (res) => {

                    expect(res.statusCode).to.be.equal(201);
                    expect(res.headers.location).to.include('/photos/');
                    expect(res.result.id).to.be.a.number();
                    return done();
                });
            });
        });
    });
});
