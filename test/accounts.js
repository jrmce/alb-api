'use strict';

const Lab = require('lab');

const Util = require('./util');

const lab = exports.lab = Lab.script();
const before = lab.before;
const afterEach = lab.afterEach;
const describe = lab.experiment;
const it = lab.test;
const expect = require('code').expect;

describe('Accounts handler', () => {

    let server;

    before((done) =>  {

        Util.createServer((s) => {

            server = s;
            return done();
        });
    });

    afterEach((done) => {

        Util.cleanDb(() => {

            return done();
        });
    });

    describe('creating an account', () => {

        it('should create a new account', (done) => {

            server.inject({
                method: 'POST',
                url: '/accounts',
                payload: {
                    email: 'test1@example.com',
                    password: 'secret'
                }
            }, (res) => {

                expect(res.statusCode).to.be.equal(201);
                expect(res.headers.location).to.include('/accounts/');
                expect(res.result.id).to.be.a.number();
                return done();
            });
        });

        it('should require an email', (done) => {

            server.inject({
                method: 'POST',
                url: '/accounts',
                payload: {
                    password: 'secret'
                }
            }, (res) => {

                expect(res.statusCode).to.be.equal(400);
                return done();
            });
        });

        it('should require a password', (done) => {

            server.inject({
                method: 'POST',
                url: '/accounts',
                payload: {
                    email: 'test@example.com'
                }
            }, (res) => {

                expect(res.statusCode).to.be.equal(400);
                return done();
            });
        });

        it('should throw when creating a duplicate account', (done) => {

            const email = 'test2@example.com';

            server.inject({
                method: 'POST',
                url: '/accounts',
                payload: {
                    email,
                    password: 'secret'
                }
            }, (res) => {

                server.inject({
                    method: 'POST',
                    url: '/accounts',
                    payload: {
                        email,
                        password: 'secret'
                    }
                }, (response) => {

                    expect(response.statusCode).to.be.equal(422);
                    return done();
                });
            });
        });
    });

    describe('deleting an account', () => {

        it('should delete an account', (done) => {

            server.inject({
                method: 'POST',
                url: '/accounts',
                payload: {
                    email: 'test3@example.com',
                    password: 'secret'
                }
            }, (res) => {

                server.inject({
                    method: 'DELETE',
                    url: res.headers.location,
                    credentials: {}
                }, (result) => {

                    expect(result.statusCode).to.be.equal(200);
                    expect(result.result).to.be.equal({ status: 'ok' });
                    return done();
                });
            });
        });
    });
});
