'use strict';

const Bcrypt = require('bcrypt');
const Moment = require('moment');
const Boom = require('boom');

const Db = require('../../db');

const internals = {
    SALT_ROUNDS: 8
};

module.exports = function (email, password, cb) {

    Bcrypt.hash(password, internals.SALT_ROUNDS, (err, digest) => {

        if (err) {
            return cb(err);
        }

        const sql = 'INSERT INTO accounts SET ?';
        const now = Moment().unix();
        const account = {
            email,
            digest,
            createdAt: now,
            updatedAt: now
        };

        Db.query(sql, account, (err, result) => {

            if (err) {

                if (err.code === 'ER_DUP_ENTRY') {
                    return cb(Boom.badData('Account already exists.'));
                }

                return cb(err);
            }

            return cb(null, { id: result.insertId });
        });
    });
};
