'use strict';

const Db = require('../../db');

module.exports = function (id, cb) {

    const sql = 'DELETE FROM accounts WHERE id = ?';

    Db.query(sql, [id], (err, res) => {

        if (err) {
            return cb(err);
        }

        return cb(null);
    });
};
