'use strict';

const Bcrypt = require('bcrypt');
const Boom = require('boom');

const Db = require('../../db');

module.exports = function (email, password, cb) {

    const sql = 'SELECT DISTINCT(email), digest, createdAt, updatedAt FROM accounts WHERE email = ?';

    return Db.query(sql, [email], (err, result) => {

        if (err) {
            return cb(err);
        }

        if (result.length < 1) {
            return cb(Boom.badData('Account not found.'));
        }

        const account = result[0];
        const digest = account.digest.toString('utf8');

        Bcrypt.compare(password, digest, (err, isValid) => {

            if (err) {
                return cb(err);
            }

            delete account.digest;

            return cb(null, isValid, account);
        });
    });
};
