'use strict';

const Moment = require('moment');
const Fs = require('fs');
const Uuid = require('node-uuid');
const Path = require('path');
const FileType = require('file-type');

const Db = require('../../db');

const extractDataOnly = function (base64String) {
    // Check if this is a javascript FileReader image.
    // If so, it probably has some extra metadata
    if (base64String.indexOf(',') > -1) {
        return base64String.split(',')[1];
    }

    return base64String;
};

module.exports = function (data, cb) {

    const binaryData = new Buffer(extractDataOnly(data), 'base64');
    const filetype = FileType(binaryData);
    const originalFile = Uuid.v4();
    const thumbnailFile = Uuid.v4();
    const size = binaryData.byteLength;
    const photosPath = Path.resolve('lib', 'photos');

    const saveToDb = function (err) {

        if (err) {
            return cb(err);
        }

        const sql = 'INSERT INTO photos SET ?';
        const now = Moment().unix();
        const photo = {
            size,
            originalFile,
            thumbnailFile,
            createdAt: now,
            updatedAt: now
        };

        return Db.query(sql, photo, (err, result) => {

            if (err) {
                return cb(err);
            }

            return cb(null, { id: result.insertId });
        });
    };

    const writeThumbnail = function (err) {

        if (err) {
            return cb(err);
        }

        return Fs.writeFile(Path.resolve(photosPath, `${thumbnailFile}.${filetype.ext}`), binaryData, saveToDb);
    };

    const writeOriginal = function () {

        return Fs.writeFile(Path.resolve(photosPath, `${originalFile}.${filetype.ext}`), binaryData, writeThumbnail);
    };

    return writeOriginal();
};
