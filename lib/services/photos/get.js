'use strict';

const Db = require('../../db');

module.exports = function (id, cb) {

    const sql = 'SELECT * FROM photos WHERE id = ?';

    Db.query(sql, [id], (err, res) => {

        if (err) {
            return cb(err);
        }

        if (res.length < 1) {
            return cb(Boom.badData('Photo not found.'));
        }

        return cb(null, res[0]);
    });
};
