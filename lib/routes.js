'use strict';

const Accounts = require('./handlers/accounts');
const Health = require('./handlers/health');
const Photos = require('./handlers/photos');

exports.endpoints = [
    // Health
    { method: 'GET', path: '/health', config: Health.check },
    { method: 'GET', path: '/health/auth', config: Health.auth },

    // Accounts
    { method: 'POST', path: '/accounts', config: Accounts.create },
    { method: 'DELETE', path: '/accounts/{id}', config: Accounts.destroy },
    { method: 'GET', path: '/me', config: Accounts.me },

    // Photos
    { method: 'POST', path: '/photos', config: Photos.create },
    { method: 'GET', path: '/photos/{id}', config: Photos.get },
    { method: 'DELETE', path: '/photos/{id}', config: Photos.destroy }
];
