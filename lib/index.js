'use strict';

const HapiAuthBasic = require('hapi-auth-basic');

const Methods = require('./methods');
const Routes = require('./routes');
const AccountsService = require('./services/accounts');

exports.register = (server, options, next) => {

    server.decorate('reply', 'success', Methods.success);
    server.decorate('reply', 'error', Methods.error);
    server.decorate('reply', 'collection', Methods.collection);

    server.register([HapiAuthBasic], (err) => {

        if (err) {
            return next(err);
        }

        server.auth.strategy('basic', 'basic', true, {
            validateFunc: function (req, email, password, cb) {

                return AccountsService.authenticate(email, password, cb);
            }
        });

        server.route(Routes.endpoints);

        return next();
    });
};

exports.register.attributes = {
    pkg: require('../package.json')
};
