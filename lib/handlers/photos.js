'use strict';

const Joi = require('joi');

const PhotosService = require('../services/photos');

exports.create = {
    validate: {
        payload: {
            data: Joi.string().required()
        }
    },
    payload: {
        maxBytes: 1073741824
    },
    handler: function (request, reply) {

        return PhotosService.create(request.payload.data, (err, result) => {

            if (err) {
                return reply.error(err);
            }

            return reply(result).created(`/photos/${result.id}`);
        });
    }
};

exports.destroy = {
    validate: {
        params: {
            id: Joi.number().required()
        }
    },
    handler: function (request, reply) {

        return PhotosService.delete(request.params.id, (err, result) => {

            if (err) {
                return reply.error(err);
            }

            return reply.success();
        });
    }
};

exports.get = {
    validate: {
        params: {
            id: Joi.number().required()
        }
    },
    handler: function (request, reply) { }
};
