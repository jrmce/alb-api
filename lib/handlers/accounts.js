'use strict';

const Joi = require('joi');

const AccountsService = require('../services/accounts');

exports.create = {
    auth: false,
    validate: {
        payload: {
            email: Joi.string().email().required(),
            password: Joi.string().required()
        }
    },
    handler: function (request, reply) {

        return AccountsService.create(request.payload.email, request.payload.password, (err, result) => {

            if (err) {
                return reply.error(err);
            }

            return reply(result).created(`/accounts/${result.id}`);
        });
    }
};

exports.destroy = {
    validate: {
        params: {
            id: Joi.number().required()
        }
    },
    handler: function (request, reply) {

        return AccountsService.delete(request.params.id, (err, result) => {

            if (err) {
                return reply.error(err);
            }

            return reply.success();
        });
    }
};

exports.me = {
    handler: function (request, reply) {

        return reply(request.auth.credentials);
    }
};
