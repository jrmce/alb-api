'use strict';

const MySql = require('mysql');

const internals = {};
internals.pool = null;

internals.getPool = function () {

    if (internals.pool) {
        return internals.pool;
    }

    internals.pool = MySql.createPool({
        host     : 'localhost',
        user     : 'alb',
        password : 'password',
        database : `alb-${process.env.NODE_ENV}`
    });

    return internals.pool;
};

module.exports = internals.getPool();
